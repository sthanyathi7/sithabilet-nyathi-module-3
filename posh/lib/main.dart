import 'package:flutter/material.dart';
import './screens/screen_index.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // to configure the routes
        title: 'Posh Eminence - Self Care',
        theme: ThemeData(
          primarySwatch: Colors.teal,
        ),
        routes: {
          '/': (context) => const HomePage(),
          '/d': (context) => const Dashboard(),
          '/r': (context) => const Registration(),
          '/l': (context) => const Login(),
          '/e': (context) => const Edit(),
          '/p': (context) => const PromotionsPage(),
        });
  }
}
