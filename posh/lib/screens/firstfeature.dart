import 'package:flutter/material.dart';
import 'screen_index.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  static const String companyDescription =
      "Established in 2022, POSH EMINENCE is the best beauty salon in the industry. POSH EMINENCE offers everything you need to be looking good all the time. At Posh Eminence weunderstands how the perfect hair cut can not only transform a person\’s lookbut also enhance self-esteem and confidence. This is why we always cuthair to suit clients\’ face shape and choose colours to suit skin tone. We  strives to enhance our clients\’ best features while giving them a professional but easy-to-maintain style. As a highly versatile hair stylists, we have extensive experience with brides, catwalk cuts, high-fashion styles and even edgy asymmetrical looks";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Posh Eminence"),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(38.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const Image(
                    image: AssetImage('images/saloon09.png'),
                    height: 180,
                  ),
                  Container(
                    // ignore: unnecessary_const
                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 30),
                    child: const Text(companyDescription,
                        // overflow: TextOverflow.visible,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            height:
                                1.7, // the height between text, default is 1.0
                            letterSpacing:
                                1.0 // the white space between letter, default is 0.0
                            )),
                  ),
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                            style: ElevatedButton.styleFrom(
                              // Foreground color
                              onPrimary: Theme.of(context)
                                  .colorScheme
                                  .onSecondaryContainer,
                              // Background color
                              primary: Theme.of(context)
                                  .colorScheme
                                  .secondaryContainer,
                            ).copyWith(
                                elevation: ButtonStyleButton.allOrNull(0.0)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Login()),
                              );
                            },
                            child: const Text(
                              'Login',
                            ),
                          ),
                          TextButton(
                            style: ElevatedButton.styleFrom(
                              // Foreground color
                              onPrimary: Theme.of(context)
                                  .colorScheme
                                  .onSecondaryContainer,
                              // Background color
                              primary: Theme.of(context)
                                  .colorScheme
                                  .secondaryContainer,
                            ).copyWith(
                                elevation: ButtonStyleButton.allOrNull(0.0)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Registration()),
                              );
                            },
                            child: const Text(
                              'Sign Up',
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
